import requests
import pywikibot


class Main:

    def __init__(self):
        self.site = pywikibot.Site('wikidata', 'wikidata')
        self.repo = self.site.data_repository()

        # Settings
        self.baseurl = 'www.spiegel.de'
        self.protocol = "http"
        self.badurl = self.protocol + "://" + self.baseurl
        self.resultlimit = 500
        self.searchindexnumber = 0
        self.change_P854 = True
        self.change_P2699_as_q = True
        self.change_P856 = True
        self.change_P1019 = True
        self.change_P973 = True
        self.change_P953 = True
        self.change_P2699 = True
        self.summary = "updating spiegel.de urls to https"

        j = self.query_for_pages(self.baseurl, self.protocol, self.resultlimit)
        print(j)
        self.find_matches(j)

    def query_for_pages(self, baseurl, protocol, resultlimit):
        query = {
            "action": "query",
            "format": "json",
            "prop": "",
            "list": "",
            "pageids": "",
            "generator": "exturlusage",
            "geuprotocol": protocol,
            "geuquery": baseurl,
            "geulimit": resultlimit
        }
        try:
            req = requests.get("https://wikidata.org/w/api.php", params=query)
            print(query)
            if req.status_code == 200:
                print("Connection to MediawikiAPI successfull")
                j = req.json()
                print("Number of pages found: " + str(len(j["query"]["pages"])))
                return j
            else:
                print("Connection to MediawikiAPI failed with :" + str(req.status_code))
                exit(0)
        except:
            print("Connection to MediawikiAPI failed, check your Internet connection")
            exit(0)

    def find_matches(self, j):
        for page in j["query"]["pages"]:
            self.searchindexnumber += 1
            qid = j["query"]["pages"][page]["title"]
            if qid.startswith("Q"):
                item = pywikibot.ItemPage(self.repo, qid)
                item.get()
                print(item.full_url())
                if item.claims:
                    for claim in item.claims:
                        realclaim = item.claims[claim]
                        for value in realclaim:
                            self.fix_urls(value)

    def fix_urls(self, claimvalue):
        if claimvalue.type == "url" and self.badurl in claimvalue.target:
            if (claimvalue.id == "P856" and self.change_P856) \
                    or (claimvalue.id == "P1019" and self.change_P1019) \
                    or (claimvalue.id == "P973" and self.change_P973) \
                    or (claimvalue.id == "P2699" and self.change_P2699) \
                    or (claimvalue.id == "P953" and self.change_P953):
                claimvalue = self.replace_target(claimvalue)
                self.repo.changeClaimTarget(claimvalue, bot=False, summary=self.summary)
                return

        if self.change_P854 or self.change_P2699_as_q:
            for source in claimvalue.sources:
                if "P854" in source and self.change_P854:
                    urlclaim = source["P854"][0]
                    url = urlclaim.target
                    if self.badurl in url:
                        self.replace_target(urlclaim)
                        srcclaims = []
                        for otherclaims in source.values():
                            srcclaims.append(otherclaims[0])

                        self.repo.editSource(claimvalue, srcclaims, new=False, bot=False,
                                             summary=self.summary)
                        return

                if "P2699" in source and self.change_P2699_as_q:
                    urlclaim = source["P2699"][0]
                    url = urlclaim.target
                    if self.badurl in url:
                        claimvalue = self.replace_target(claimvalue)
                        srcclaims = []
                        for otherclaims in source.values():
                            srcclaims.append(otherclaims[0])

                        self.repo.editSource(claimvalue, srcclaims, new=False, bot=False,
                                             summary=self.summary)
                        return

    def replace_target(self, claimvalue):
        print("found: " + claimvalue.target)
        newurl = claimvalue.target.replace("http://", "https://")
        print("replacing with: " + newurl)
        claimvalue.setTarget(newurl)
        return claimvalue


main = Main()
